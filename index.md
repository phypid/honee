# HONEE: High-Order Navier-stokes Equation Evaluator

```{include} README.md
:start-after: "<!-- abstract -->"
:end-before: "# Getting Started"
```

```{toctree}
:caption: Contents
:maxdepth: 2

doc/intro
doc/runtime_options
doc/theory
Contributing <CONTRIBUTING>
Release Notes <CHANGELOG>
```

# Indices and tables

- {ref}`genindex`
- {ref}`search`

```{bibliography}
```
